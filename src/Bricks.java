public class Bricks {

    public static void main(String[] args) {
        Bricks brick = new Bricks();
        System.out.println(brick.makeBricks(3,2, 8));
    }

    public boolean makeBricks(int small, int big, int goal) {
        int rest5Inch = goal % 5;
        int howLong5Inch;
        int need5Inch = (goal - rest5Inch)/5;
        if (need5Inch >= big) howLong5Inch = 5 * big;
        else howLong5Inch = need5Inch;
        if(howLong5Inch == goal || (howLong5Inch < goal && howLong5Inch + small >= goal)) return true;
        return false;
    }

}
